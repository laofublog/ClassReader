package org.rz.area;

public class Slot {
	private int value;
	private Object ref;
	
	public Slot(int value) {
		this.value = value;
	}
	public Slot(Object ref) {
		this.ref = ref;
	}
	
	public int getValue() {
		return value;
	}
	
	
	
	public Object getRef() {
		return ref;
	}
	
	
}
