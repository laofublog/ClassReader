package org.rz.area;

import org.rz.parseclass.ClassParseInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Frame {
	
	private List<Slot> localVars;
	private Stack<Slot> operateStack;
	private int maxLocalVars;
	private int maxStack;
	private final ClassParseInfo classParseInfo;
	
	public Frame(int maxLocalVars, int maxStack, ClassParseInfo classParseInfo) {
		this.maxLocalVars = maxLocalVars;
		this.maxStack = maxStack;
		localVars = new ArrayList<>(maxLocalVars);
		this.classParseInfo = classParseInfo;
		operateStack = new Stack<>();
	}
	
	public void setVars(int index,Slot slot) {
		this.localVars.add(slot);
	}
	public Slot getVar(int index) {
		return localVars.get(index - 1);
	}
	
	public void pushVar(Slot slot) {
		operateStack.push(slot);
	}
	
	public Slot pop() {
		if (this.operateStack.isEmpty())
			return null;
		return this.operateStack.pop();
	}
	
	public ClassParseInfo getClassParseInfo() {
		return classParseInfo;
	}
}
