package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.area.Slot;

public class GetStatic implements Instruction {
	@Override
	public void execute(Frame frame) {
		frame.pushVar(new Slot(System.out));
	}
}
