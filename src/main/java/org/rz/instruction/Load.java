package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.area.Slot;

public class Load implements Instruction {
	public Load(int index) {
		this.index = index;
	}
	
	private final int index;
	
	@Override
	public void execute(Frame frame) {
		Slot var = frame.getVar(index);
		frame.pushVar(var);
	}

}
