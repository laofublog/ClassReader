package org.rz.instruction;

import org.rz.area.Frame;

public interface Instruction {
	
	void execute(Frame frame);
}
