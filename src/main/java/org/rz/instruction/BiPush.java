package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.area.Slot;

public class BiPush implements Instruction {
	private final int value;
	
	public BiPush(int value) {
		this.value = value;
	}
	
	@Override
	public void execute(Frame frame) {
		frame.pushVar(new Slot(value));
	}
}
