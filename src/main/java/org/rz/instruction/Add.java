package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.area.Slot;

public class Add implements Instruction {
	@Override
	public void execute(Frame frame) {
		Slot var1 = frame.pop();
		Slot var2 = frame.pop();
		int var3 = var1.getValue() + var2.getValue();
		frame.pushVar(new Slot(var3));
		
	}
}
