package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.area.Slot;

public class IConst implements Instruction {
	private int value;
	
	public IConst(int value) {
		this.value = value;
	}
	
	@Override
	public void execute(Frame frame) {
		pushValue(frame,value);
	}
	
	public void pushValue(Frame frame, int value) {
		frame.pushVar(new Slot(value));
	}
	
}
