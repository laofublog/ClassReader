package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.parseclass.Method;
import org.rz.parseclass.constantpool.ConstantPool;
import org.rz.parseclass.constantpool.constanttype.CONSTANT_Methodref_info;
import org.rz.parseclass.constantpool.constanttype.CONSTANT_NameAndType_info;
import org.rz.parseclass.constantpool.constanttype.CONSTANT_Utf8_info;

public class Invokestatic implements Instruction {
	private final int methodIndex;
	
	public Invokestatic(int methodIndex) {
		this.methodIndex = methodIndex;
	}
	
	//todo:还未完成
	@Override
	public void execute(Frame frame) {
		ConstantPool constant_pool = frame.getClassParseInfo().constant_pool;
		CONSTANT_Methodref_info methodrefInfo = (CONSTANT_Methodref_info) constant_pool.getConstantByIndex(methodIndex);
		CONSTANT_NameAndType_info nameAndTypeInfo = (CONSTANT_NameAndType_info)constant_pool.getConstantByIndex(methodrefInfo.getName_and_type_index());
		int name_inex = nameAndTypeInfo.getName_inex();
		String methodName=constant_pool.getUtf8Constant(name_inex).getText();
		Method[] methods = frame.getClassParseInfo().methods;
		for (int i = 0; i < methods.length; i++) {
			int name_index = methods[i].getName_index();
			CONSTANT_Utf8_info utf8Constant = constant_pool.getUtf8Constant(name_index);
			if(!utf8Constant.getText().equalsIgnoreCase(methodName)){
				continue;
			}
			
			
		}
		
		
	}
}
