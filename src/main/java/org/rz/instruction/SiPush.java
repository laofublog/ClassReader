package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.area.Slot;

public class SiPush implements Instruction {
	private final int value;
	
	public SiPush(int value) {
		this.value = value;
	}
	
	@Override
	public void execute(Frame frame) {
		frame.pushVar(new Slot(value));
	}
}
