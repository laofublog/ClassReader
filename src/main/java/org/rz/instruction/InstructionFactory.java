package org.rz.instruction;

import org.rz.parseclass.attributes.Opcode;

import java.io.DataInputStream;
import java.io.IOException;

public class InstructionFactory {
	public static Instruction getInstruction(Opcode opcode, DataInputStream dataInputStream) throws IOException {
		switch (opcode) {
			case iconst_1: {
				return new IConst(1);
			}
			case iconst_2:
				return new IConst(2);
			case iload_1:
				return new Load(1);
			case iload_2:
				return new Load(2);
			case iload_3:
				return new Load(3);
			case istore_1:
				return new Store(1);
			case istore_2:
				return new Store(2);
			case istore_3:
				return new Store(3);
			case iadd:
				return new Add();
			case _return:
				return new Return();
			case getstatic:
				return new GetStatic();
			case invokevirtual:
				return new InvokeVirtual();
			case bipush:
				return new BiPush(dataInputStream.readUnsignedByte());
			case sipush:
				return new SiPush(dataInputStream.readUnsignedShort());
			case invokestatic:
				return new Invokestatic(dataInputStream.readUnsignedShort());
			default: {
				System.out.println(opcode + "未实现");
				return null;
			}
		}
	}
}
