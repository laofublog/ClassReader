package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.area.Slot;

import java.io.PrintStream;

public class InvokeVirtual implements Instruction {
	//mock
	@Override
	public void execute(Frame frame) {
		Slot pop = frame.pop();
		Slot out = frame.pop();
		PrintStream sysOut = (PrintStream) out.getRef();
		sysOut.println(pop.getValue());
	}
}
