package org.rz.instruction;

import org.rz.area.Frame;
import org.rz.area.Slot;

public class Store implements Instruction {
	 final int index;
	
	public Store(int index) {
		this.index = index;
	}
	
	@Override
	public void execute(Frame frame) {
		setVar(frame,this.index);
	}
	
	public void setVar(Frame frame, int index) {
		Slot pop = frame.pop();
		frame.setVars(index, pop);
	}
}
