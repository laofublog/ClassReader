package org.rz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtils {
	
	public static boolean isEmpty(String value) {
		return value == null || value.length() == 0;
	}
	
	public static boolean isNotEmpty(String value) {
		return !isEmpty(value);
	}
	
	public static boolean isBlank(String value) {
		return value.trim().equals("");
	}
	
	public static boolean isContain(String source, String target, String split) {
		if (source == null || target == null) {
			return false;
		}
		List<String> sourceList = Arrays.asList(source.split(split));
		return sourceList.contains(target);
		
	}
	
	public static boolean isContain(String source, String target) {
		return isContain(source, target, ",");
	}
	
	public static String binaryToHexString(byte[] bytes) {
		String hexStr = "0123456789ABCDEF";
		StringBuilder result = new StringBuilder();
		String hex = "";
		for (int i = 0; i < bytes.length; i++) {
			//字节高4位
			hex = String.valueOf(hexStr.charAt((bytes[i] & 0xF0) >> 4));
			//字节低4位
			hex += String.valueOf(hexStr.charAt(bytes[i] & 0x0F));
			result.append(hex);
		}
		return result.toString();
	}
	
	public static String[] binaryToJVMCmd(byte[] bytes) {
		String hexStr = "0123456789ABCDEF";
		StringBuilder result = new StringBuilder();
		List<String> jvmCmdList=new ArrayList<>();
		for (int i = 0; i < bytes.length; i++) {
			//字节高4位
			String hex = String.valueOf(hexStr.charAt((bytes[i] & 0xF0) >> 4));
			//字节低4位
			hex += String.valueOf(hexStr.charAt(bytes[i] & 0x0F));
			String hexKey="0x"+hex.toLowerCase();
			jvmCmdList.add(JVMCmdData.Jvm_map.get(hexKey));
		}
		return jvmCmdList.toArray(new String[0]);
	}
}
