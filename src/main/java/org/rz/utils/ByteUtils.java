package org.rz.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteUtils {
	public static short bytesToShort(byte[] bytes) {
		
		return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getShort();
	}
	public static byte[] shortToBytes(short value) {
		return ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN).putShort(value).array();
	}
}
