package org.rz;

public class BubbleSortTest {
	public static void main(String[] args) {
		int[] arr = {10, 5, 77, 11, 95, 9, 78, 56, 36, 97, 65, 36, 10, 24, 92};
		
		bubbleSort(arr);
		printArray(arr);
	}
	
	public static void bubbleSort(int[] arr) {
		
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-i-1; j++) {
				if (arr[j] > arr[j + 1]) {
					int tmp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = tmp;
				}
			}
		}
	}
	
	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}
}


