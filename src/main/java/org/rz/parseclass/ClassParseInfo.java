package org.rz.parseclass;


import org.rz.parseclass.attributes.Attribute;
import org.rz.parseclass.attributes.AttributeFactory;
import org.rz.parseclass.constantpool.ConstantPool;

import java.util.Arrays;

public class ClassParseInfo {
	public final String magic;
	public final int minor_version;
	public final int major_version;
	public final int constantCount;
	public final ConstantPool constant_pool;
	
	public final AccessFlags access_flags;
	public final int this_class;
	public final int super_class;
	public final int[] interfaces;
	public final Field[] fields;
	public final Method[] methods;
	public final int attributeCount;
	public final Attribute[] attributes;
	
	public ClassParseInfo(String path) throws Exception {
		ClassReadCursor cursor = new ClassReadCursor(path, this);
		byte[] byteArr = new byte[4];
		cursor.readFully(byteArr);
		
	 
		magic = "cafebabe";
		minor_version = cursor.readUnsignedShort();
		major_version = cursor.readUnsignedShort();
		constantCount = cursor.readUnsignedShort();
		constant_pool = new ConstantPool(cursor, constantCount);
		access_flags = new AccessFlags(cursor);
		this_class = cursor.readUnsignedShort();
		super_class = cursor.readUnsignedShort();
		
		int interfacesCnt = cursor.readUnsignedShort();
		this.interfaces = new int[interfacesCnt];
		
		
		for (int i = 0; i < interfacesCnt; i++) {
			this.interfaces[i] = cursor.readUnsignedShort();
		}
		
		int fileCount = cursor.readUnsignedShort();
		this.fields = new Field[fileCount];
		
		for (int i = 0; i < fileCount; i++) {
			this.fields[i] = new Field(cursor);
		}
		
		int methodCount = cursor.readUnsignedShort();
		this.methods = new Method[methodCount];
		
		for (int i = 0; i < methodCount; i++) {
			this.methods[i] = new Method(cursor);
		}
		attributeCount = cursor.readUnsignedShort();
		this.attributes = new Attribute[attributeCount];
		
		for (int i = 0; i < attributeCount; i++) {
			this.attributes[i] = AttributeFactory.create(cursor);
		}
		
		
		System.out.println(this.toString());
		
		
	}
	
	
	
	@Override
	public String toString() {
		return "ClassParseInfo{" + "magic='" + magic + '\'' + ", minor_version=" + minor_version + ", major_version=" + major_version + ", constant_pool=" + constant_pool + ", access_flags=" + access_flags + ", this_class=" + this_class + ", super_class=" + super_class + ", interfaces=" + Arrays.toString(interfaces) + ", fields=" + Arrays.toString(fields) + ", methods=" + Arrays.toString(methods) + ", attributeCount=" + attributeCount + ", attributes=" + Arrays.toString(attributes) + '}';
	}
}
