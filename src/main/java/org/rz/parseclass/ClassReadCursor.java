package org.rz.parseclass;

import org.rz.StringUtils;
import org.rz.parseclass.constantpool.ConstantPool;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ClassReadCursor {
	private DataInputStream dataInputStream;
	private ClassParseInfo classParseInfo;
	
	public ClassReadCursor(String filePath, ClassParseInfo classParseInfo) {
		try {
			this.classParseInfo = classParseInfo;
			byte[] bytes = Files.readAllBytes(Paths.get(filePath));
			ByteBuffer byteBuffer = ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN);
			this.dataInputStream = new DataInputStream(new ByteArrayInputStream(byteBuffer.array()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public ClassReadCursor(byte[] code) {
		try {
			ByteBuffer byteBuffer = ByteBuffer.wrap(code).order(ByteOrder.BIG_ENDIAN);
			this.dataInputStream = new DataInputStream(new ByteArrayInputStream(byteBuffer.array()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public ConstantPool getConstantPool() {
		return this.classParseInfo.constant_pool;
	}
	
	public void readFully(byte[] byteArr) throws IOException {
		this.dataInputStream.readFully(byteArr);
	}
	
	public int readUnsignedByte() throws IOException {
		return this.dataInputStream.readUnsignedByte();
	}
	
	public int readByte() throws IOException {
		return this.dataInputStream.readByte();
	}
	
	public int readUnsignedShort() throws IOException {
			return this.dataInputStream.readUnsignedShort();
	}
	
	public int readInt() throws IOException {
		return this.dataInputStream.readInt();
	}
	
	public long readLong() throws IOException {
		return this.dataInputStream.readLong();
	}
	
	public float readFloat() throws IOException {
		return this.dataInputStream.readFloat();
	}
	
	public double readDouble() throws IOException {
		return this.dataInputStream.readDouble();
	}
	
	public String readUTF() throws IOException {
		return this.dataInputStream.readUTF();
	}
	public String readUTF(int n) throws IOException {
		byte[] byteArr = new byte[n];
		
		this.dataInputStream.readFully(byteArr);
		return StringUtils.binaryToHexString(byteArr);
	}
	
	public String readStr(int n) throws IOException {
		byte[] byteArr = new byte[n];
		
		this.dataInputStream.readFully(byteArr);
		return StringUtils.binaryToHexString(byteArr);
	}
	
}
