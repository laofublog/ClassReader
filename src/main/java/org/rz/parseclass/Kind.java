package org.rz.parseclass;

public enum Kind {
	Class,
	InnerClass,
	Field,
	Method;
}
