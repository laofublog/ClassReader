package org.rz.parseclass;

import org.rz.area.Frame;
import org.rz.instruction.Instruction;
import org.rz.instruction.InstructionFactory;
import org.rz.parseclass.attributes.Code_attribute;
import org.rz.parseclass.attributes.Opcode;
import org.rz.parseclass.constantpool.constanttype.CONSTANT_Utf8_info;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Arrays;

public class ClassReaderMain {
	public static void main(String[] args) {
		try {
			ClassParseInfo classParseInfo = new ClassParseInfo("/Users/fuwei/work/rzclassreader/target/classes/org/rz/AddMain.class");
			
			Method mainMethod = getMainMethod(classParseInfo);
			Code_attribute code = (Code_attribute) Arrays.stream(mainMethod.getAttributes()).filter(x -> x.tagName.equalsIgnoreCase("code")).findFirst().orElse(null);
			
			Frame frame = new Frame(code.getMax_locals(), code.getMax_stack(), classParseInfo);
			
			DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(code.getParaCode()));
			for (Opcode op : code.getOpcodeArr()) {
				if (op == null)
					continue;
				
				Instruction instruction = InstructionFactory.getInstruction(op, dataInputStream);
				instruction.execute(frame);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static Method getMainMethod(ClassParseInfo classParseInfo) {
		Method[] methods = classParseInfo.methods;
		for (int i = 0; i < methods.length; i++) {
			int name_index = methods[i].getName_index();
			CONSTANT_Utf8_info utf8Constant = classParseInfo.constant_pool.getUtf8Constant(name_index);
			if (utf8Constant.getText().equalsIgnoreCase("main")) {
				return methods[i];
			}
		}
		return null;
	}
	
	
}
