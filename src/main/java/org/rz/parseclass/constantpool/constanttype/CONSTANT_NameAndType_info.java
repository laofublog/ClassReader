package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_NameAndType_info extends CPInfo {
	private int name_inex;
	private int descriptor_index;
	
	public int getName_inex() {
		return name_inex;
	}
	
	public int getDescriptor_index() {
		return descriptor_index;
	}
	
	public CONSTANT_NameAndType_info(ClassReadCursor cusor) throws IOException {
		super(ConstantPoolType.CONSTANT_NameAndType);
		this.name_inex = cusor.readUnsignedShort();
		this.descriptor_index = cusor.readUnsignedShort();
	}
}
