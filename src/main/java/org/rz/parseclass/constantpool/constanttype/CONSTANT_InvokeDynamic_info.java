package org.rz.parseclass.constantpool.constanttype;

import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_InvokeDynamic_info extends CPInfo {
 
	private int bootstrap_method_attr_index;
	private int name_and_type_index;
	
	public CONSTANT_InvokeDynamic_info(ClassReadCursor cursor) throws IOException {
		super(ConstantPoolType.CONSTANT_InvokeDynamic);
		bootstrap_method_attr_index = cursor.readUnsignedShort();
		name_and_type_index = cursor.readUnsignedShort();
	}
}
