package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_String_info  extends CPInfo {
 
	private  int string_index;

	public CONSTANT_String_info(ClassReadCursor cusor) throws IOException {
		super(ConstantPoolType.CONSTANT_String);
		this.string_index = cusor.readUnsignedShort();
	}
}
