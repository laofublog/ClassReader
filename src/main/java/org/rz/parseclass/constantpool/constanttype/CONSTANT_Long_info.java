package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_Long_info extends CPInfo {
	private long longValue;
	
	
	public CONSTANT_Long_info(ClassReadCursor cursor) throws IOException {
		super(ConstantPoolType.CONSTANT_Long);
		longValue = cursor.readLong();
	}
}
