package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_Float_info extends CPInfo {
    private float value;
    
    public CONSTANT_Float_info(ClassReadCursor cursor) throws IOException {
        super(ConstantPoolType.CONSTANT_Float);
        value=cursor.readFloat();
        
    }
}
