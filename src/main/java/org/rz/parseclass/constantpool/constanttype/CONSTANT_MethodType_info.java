package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_MethodType_info  extends CPInfo
{
	private int descriptor_index;
	
	public int getDescriptor_index() {
		return descriptor_index;
	}
	
	public CONSTANT_MethodType_info(ClassReadCursor cursor) throws IOException {
		super(ConstantPoolType.CONSTANT_MethodType);
		descriptor_index=cursor.readUnsignedShort();
	}
}
