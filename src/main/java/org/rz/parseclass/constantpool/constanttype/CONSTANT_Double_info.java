package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_Double_info extends CPInfo {
	private double douleValue;
	public CONSTANT_Double_info(ClassReadCursor cursor) throws IOException {
		super(ConstantPoolType.CONSTANT_Double);
		this.douleValue = cursor.readDouble();
	}
}
