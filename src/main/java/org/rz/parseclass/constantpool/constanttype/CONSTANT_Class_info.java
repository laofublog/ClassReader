package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_Class_info  extends CPInfo {
	private int name_index;
	
	public CONSTANT_Class_info(ClassReadCursor cursor) throws IOException {
		super(ConstantPoolType.CONSTANT_Class);
		name_index=cursor.readUnsignedShort();
	}
}
