package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_MethodHandle_info  extends CPInfo {
    private int reference_kind;
    private int reference_index;
    
    
    public CONSTANT_MethodHandle_info(ClassReadCursor cursor) throws IOException {
        super(ConstantPoolType.CONSTANT_MethodHandle);
        reference_kind=cursor.readUnsignedByte();
        reference_index=cursor.readUnsignedShort();
    }
}
