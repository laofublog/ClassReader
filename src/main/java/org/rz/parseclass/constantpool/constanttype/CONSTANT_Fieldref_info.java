package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_Fieldref_info  extends CPInfo {
	
	private int class_index;
	private int name_and_type_index;
	
	
	public CONSTANT_Fieldref_info(ClassReadCursor cursor) throws IOException {
		super(ConstantPoolType.CONSTANT_Fieldref);
	    class_index=cursor.readUnsignedShort();
	    name_and_type_index=cursor.readUnsignedShort();
	}
}
