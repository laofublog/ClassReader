package org.rz.parseclass.constantpool.constanttype;


import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.CPInfo;
import org.rz.parseclass.constantpool.ConstantPoolType;

import java.io.IOException;

public class CONSTANT_Utf8_info  extends CPInfo {

    private  int length;
	private  String text;
	
	public CONSTANT_Utf8_info(ClassReadCursor cusor) throws IOException {
		super(ConstantPoolType.CONSTANT_Utf8);
//		length=cusor.readUnsignedShort();
		text=cusor.readUTF();
 
	}
	
	public String getText() {
		return text;
	}
}
