package org.rz.parseclass.constantpool;

import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.constantpool.constanttype.*;

import static org.rz.parseclass.constantpool.ConstantPoolType.*;

public class ConstantPool {
	private CPInfo[] cpInfos;
	private final int count;
	
	
	public ConstantPool(ClassReadCursor cursor, int count) throws Exception {
		this.count = count;
		cpInfos = new CPInfo[count];
		init(cursor);
		
	}
	
	public void init(ClassReadCursor cursor) throws Exception {
		for (int i = 1; i < count; i++) {
			int tag = cursor.readByte();
			switch (tag) {
				case CONSTANT_Utf8: {
					cpInfos[i] = new CONSTANT_Utf8_info(cursor);
					break;
				}
				case CONSTANT_Integer: {
					cpInfos[i] = new CONSTANT_Integer_info(cursor);
					break;
				}
				case CONSTANT_Float: {
					cpInfos[i] = new CONSTANT_Float_info(cursor);
					break;
				}
				case CONSTANT_Long: {
					cpInfos[i] = new CONSTANT_Long_info(cursor);
					i++;
					break;
				}
				case CONSTANT_Double: {
					cpInfos[i] = new CONSTANT_Double_info(cursor);
					i++;
					break;
				}
				case CONSTANT_String: {
					cpInfos[i] = new CONSTANT_String_info(cursor);
					break;
				}
				case CONSTANT_Fieldref: {
					cpInfos[i] = new CONSTANT_Fieldref_info(cursor);
					break;
				}
				
				case CONSTANT_Methodref: {
					cpInfos[i] = new CONSTANT_Methodref_info(cursor);
					break;
				}
				case CONSTANT_InterfaceMethodref: {
					cpInfos[i] = new CONSTANT_InterfaceMethodref_info(cursor);
					break;
				}
				case CONSTANT_NameAndType: {
					cpInfos[i] = new CONSTANT_NameAndType_info(cursor);
					break;
				}
				case CONSTANT_Class: {
					cpInfos[i] = new CONSTANT_Class_info(cursor);
					break;
				}
				case CONSTANT_MethodHandle: {
					cpInfos[i] = new CONSTANT_MethodHandle_info(cursor);
					break;
				}
				case CONSTANT_MethodType: {
					cpInfos[i] = new CONSTANT_MethodType_info(cursor);
					break;
				}
				case CONSTANT_InvokeDynamic: {
					cpInfos[i] = new CONSTANT_InvokeDynamic_info(cursor);
					break;
				}
				default: {
					throw new Exception("未实现的类型i：" + i + ",tag:" + tag);
				}
				
			}
		}
	}
	
	
	public CPInfo getUtf8Constant(String name) {
		for (int i = 0; i < cpInfos.length; i++) {
			CPInfo cpInfo = cpInfos[i];
			if (cpInfo.getTag() != CONSTANT_Utf8) {
				continue;
			}
			CONSTANT_Utf8_info constant_utf8_info = (CONSTANT_Utf8_info) cpInfo;
			if (constant_utf8_info.getText().equals(name)) {
				return constant_utf8_info;
			}
		}
		return null;
	}
	
	public CONSTANT_Utf8_info getUtf8Constant(int nameIndex) {
		
		return (CONSTANT_Utf8_info) getConstantByIndex(nameIndex);
	}
	
	public CPInfo getConstantByIndex(int index) {
		return this.cpInfos[index];
	}
}
