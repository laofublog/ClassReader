package org.rz.parseclass.attributes;

import org.rz.parseclass.ClassReadCursor;

import java.io.IOException;

import static org.rz.parseclass.attributes.AttributeType.SourceFile;

public class SourceFile_attribute extends Attribute {
	
	
	public SourceFile_attribute(ClassReadCursor cursor, int attrTag) throws IOException {
		super(SourceFile, attrTag, cursor.readUnsignedShort());
	}
}
