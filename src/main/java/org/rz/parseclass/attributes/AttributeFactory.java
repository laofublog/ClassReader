package org.rz.parseclass.attributes;


import org.rz.parseclass.ClassReadCursor;

import static org.rz.parseclass.attributes.AttributeType.*;

public class AttributeFactory {
	
	public static Attribute create(ClassReadCursor cursor) throws Exception {
		int attrTag = cursor.readUnsignedShort();
		String attrName = cursor.getConstantPool().getUtf8Constant(attrTag).getText();
		switch (attrName) {
			case Code: {
				return new Code_attribute(cursor, attrTag);
			}
			case ConstantValue: {
				return new ConstantValue_attribute(cursor, attrTag);
			}
			case LineNumberTable: {
				return new LineNumberTable_attribute(cursor, attrTag);
			}
			case StackMapTable: {
				return new StackMapTable_attribute(cursor, attrTag);
			}
			case LocalVariableTable: {
				return new LocalVariableTable_attribute(cursor, attrTag);
			}
			case SourceFile:{
				return new SourceFile_attribute(cursor,attrTag);
			}
			
			default: {
				System.out.println(attrName + ":未实现");
			}
		}
		return null;
	}
}
