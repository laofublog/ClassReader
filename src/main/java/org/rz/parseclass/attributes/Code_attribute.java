package org.rz.parseclass.attributes;

import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.attributes.exception.ExceptionTable;

public class Code_attribute extends Attribute {
	private int max_stack;
	private int max_locals;
	private int code_length;
	private int exception_table_length;
	private int attributes_count;
	private byte[] code;
	private byte[] paraCode;
	private Opcode[] opcodeArr;
	private ExceptionTable[] exceptionTables;
	private Attribute[] attributes;
	
	public int getMax_stack() {
		return max_stack;
	}
	
	public int getMax_locals() {
		return max_locals;
	}
	
	public byte[] getParaCode() {
		return paraCode;
	}
	
	public Opcode[] getOpcodeArr() {
		return opcodeArr;
	}
	
	public Code_attribute(ClassReadCursor cursor, int attrTag) throws Exception {
		super(AttributeType.Code, attrTag, cursor.readInt());
		max_stack = cursor.readUnsignedShort();
		max_locals = cursor.readUnsignedShort();
		code_length = cursor.readInt();
		
		if (code_length > 0) {
			code = new byte[code_length];
			cursor.readFully(code);
			opcodeArr = new Opcode[code_length];
			paraCode=new byte[code_length];
		}
		int index=0;
		int paraIndex=0;
		for (int i = 0; i < code_length; i++) {
			byte bCode = code[i];
			Opcode opcode = Opcode.valueOf(Byte.toUnsignedInt(bCode));
			opcodeArr[index++] = opcode;
			if(opcode.operandCount>0){
				for (int j = 0; j < opcode.operandCount; j++) {
					paraCode[paraIndex++]=code[i+j+1];
				}
			}
			i += opcode.operandCount;
			
		}
 
		exception_table_length = cursor.readUnsignedShort();
		if (exception_table_length > 0) {
			exceptionTables = new ExceptionTable[exception_table_length];
			for (int i = 0; i < exception_table_length; i++) {
				int start_pc = cursor.readUnsignedShort();
				int end_pc = cursor.readUnsignedShort();
				int handler_pc = cursor.readUnsignedShort();
				int catch_type = cursor.readUnsignedShort();
				this.exceptionTables[i] = new ExceptionTable(start_pc, end_pc, handler_pc, catch_type);
			}
			
		}
		attributes_count = cursor.readUnsignedShort();
		if (attributes_count > 0) {
			attributes = new Attribute[attributes_count];
			for (int i = 0; i < attributes_count; i++) {
				Attribute attribute = AttributeFactory.create(cursor);
				attributes[i] = attribute;
			}
		}
	}
}
