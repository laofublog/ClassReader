package org.rz.parseclass.attributes;

import org.rz.parseclass.ClassReadCursor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.rz.parseclass.attributes.AttributeType.LocalVariableTable;

public class LocalVariableTable_attribute extends Attribute {
	private int attribute_length;
	private int local_variable_table_length;
	private List<local_variable_table> local_variable_tables;
	
	public LocalVariableTable_attribute(ClassReadCursor cusor, int attrTag) throws IOException {
		super(LocalVariableTable, attrTag, cusor.readInt());
		local_variable_table_length = cusor.readUnsignedShort();
		if (local_variable_table_length > 0) {
			local_variable_tables = new ArrayList<>();
			for (int i = 0; i < local_variable_table_length; i++) {
				int start_pc = cusor.readUnsignedShort();
				int length = cusor.readUnsignedShort();
				int name_index = cusor.readUnsignedShort();
				int descriptor_index = cusor.readUnsignedShort();
				int index = cusor.readUnsignedShort();
				local_variable_tables.add(new local_variable_table(start_pc, length, name_index, descriptor_index, index));
			}
		}
	}
}

class local_variable_table {
	private int start_pc;
	private int length;
	private int name_index;
	private int descriptor_index;
	private int index;
	
	public local_variable_table(int start_pc, int length, int name_index, int descriptor_index, int index) {
		this.start_pc = start_pc;
		this.length = length;
		this.name_index = name_index;
		this.descriptor_index = descriptor_index;
		this.index = index;
	}
}

