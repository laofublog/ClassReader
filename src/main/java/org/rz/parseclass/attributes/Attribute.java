package org.rz.parseclass.attributes;

import org.rz.parseclass.ClassReadCursor;

public class Attribute {
	
	public final String tagName;
	public final int attribute_name_index;
	public final int attribute_length;
	
	public Attribute(String tagName, int attribute_name_index, int attribute_length) {
		this.tagName = tagName;
		this.attribute_name_index = attribute_name_index;
		this.attribute_length = attribute_length;
	}
	
	public Attribute(ClassReadCursor  cursor) {
		this("",0,0);
	}
	
	public String getTagName() {
		return tagName;
	}
	
	public int getAttribute_name_index() {
		return attribute_name_index;
	}
	
	public int getAttribute_length() {
		return attribute_length;
	}
}
