package org.rz.parseclass.attributes.typeinfo;

import org.rz.parseclass.ClassReadCursor;

import java.io.IOException;

public class VerificationTypeInfo {
	private final int tag;
	private int offset;
	
	public VerificationTypeInfo(int tag) {
		this.tag = tag;
	}
	
	public VerificationTypeInfo(int tag, ClassReadCursor cursor) throws IOException {
		this.tag = tag;
		offset = cursor.readUnsignedShort();
	}
}
