package org.rz.parseclass.attributes.typeinfo;

import org.rz.parseclass.ClassReadCursor;

import java.io.IOException;

public class VerificationFactory {
	
	public static VerificationTypeInfo create(int tag, ClassReadCursor cursor) throws IOException {
		
		switch (tag) {
			case VerificationType.ITEM_Top:
			case VerificationType.ITEM_Integer:
			case VerificationType.ITEM_Float:
			case VerificationType.ITEM_Long:
			case VerificationType.ITEM_Double:
			case VerificationType.ITEM_Null:
			case VerificationType.ITEM_UninitializedThis: {
				return new VerificationTypeInfo(tag);
			}
			case VerificationType.ITEM_Object:
			case VerificationType.ITEM_Uninitialized:
			 {
				return new VerificationTypeInfo(tag,cursor);
			}
		}
		
		return new UndifineVerficationTypeInfo(tag);
		
	}
}
