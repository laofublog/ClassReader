package org.rz.parseclass.attributes;

import org.rz.parseclass.ClassReadCursor;

import java.io.IOException;

import static org.rz.parseclass.attributes.AttributeType.ConstantValue;

public class ConstantValue_attribute extends Attribute {
	private int constantvalue_index;
	private int constantvalue;
	
	public ConstantValue_attribute(ClassReadCursor cursor, int attribute_index) throws IOException {
		super(ConstantValue, attribute_index, cursor.readInt());
		constantvalue_index = cursor.readUnsignedShort();
	}
}
