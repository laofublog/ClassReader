package org.rz.parseclass.attributes;

import org.rz.parseclass.ClassReadCursor;

import java.io.IOException;

import static org.rz.parseclass.attributes.AttributeType.LineNumberTable;

public class LineNumberTable_attribute extends Attribute {
	
	private int line_number_table_length;
	private line_number_table[] line_number_table;
	
	public LineNumberTable_attribute(ClassReadCursor cursor, int attrTag) throws IOException {
		super(LineNumberTable, attrTag, cursor.readInt());
		line_number_table_length=cursor.readUnsignedShort();
		if(line_number_table_length>0){
			line_number_table=new line_number_table[line_number_table_length];
			for (int i = 0; i < line_number_table_length; i++) {
				line_number_table[i]=new line_number_table(cursor.readUnsignedShort(),cursor.readUnsignedShort());
			}
		}
	}
}

  class line_number_table {
	
	private int start_pc;
	private int line_number;
	
	public line_number_table(int start_pc, int line_number) {
		this.start_pc = start_pc;
		this.line_number = line_number;
	}
	
	public int getStart_pc() {
		return start_pc;
	}
	
	public void setStart_pc(int start_pc) {
		this.start_pc = start_pc;
	}
	
	public int getLine_number() {
		return line_number;
	}
	
	public void setLine_number(int line_number) {
		this.line_number = line_number;
	}
}

