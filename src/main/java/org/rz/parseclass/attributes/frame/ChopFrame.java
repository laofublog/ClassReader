package org.rz.parseclass.attributes.frame;

import org.rz.parseclass.ClassReadCursor;

import java.io.IOException;

public class ChopFrame extends StackMapFrame {
	private int offset_delta;
	
	public ChopFrame(short frameType, ClassReadCursor cursor) throws IOException {
		super(frameType);
		offset_delta = cursor.readUnsignedShort();
		
	}
}
