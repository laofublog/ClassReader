package org.rz.parseclass.attributes.frame;

import org.rz.parseclass.ClassReadCursor;

import java.io.IOException;

public class FrameFactory {
	
	public static StackMapFrame create(short frameType, ClassReadCursor cursor) throws IOException {
		if (frameType <= 63) {
			return new SameFrame(frameType);
		}
		if (frameType <= 127) {
			return new SameLocals1StackItemFrame(frameType, cursor);
		}
		if (frameType == 247) {
			return new SameLocals1StackItemFrameExtend(frameType, cursor);
		}
		if (frameType >= 248 && frameType <= 250) {
			return new ChopFrame(frameType, cursor);
		}
		if(frameType==251){
			return new SameFrameExtended(frameType,cursor);
		}
		if (frameType >=252 && frameType <= 254) {
			return new AppendFrame(frameType, cursor);
		}
		return new UndefineFrame(frameType);
	}
}
