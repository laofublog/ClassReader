package org.rz.parseclass.attributes.frame;

public class StackMapFrame {
	private final short frameType;
	
	public StackMapFrame(short frameType) {
		this.frameType = frameType;
	}
}
