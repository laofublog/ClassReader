package org.rz.parseclass.attributes.frame;

import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.attributes.typeinfo.VerificationFactory;
import org.rz.parseclass.attributes.typeinfo.VerificationTypeInfo;

import java.io.IOException;

public class SameLocals1StackItemFrameExtend extends StackMapFrame {
	private final VerificationTypeInfo[] verificationTypeInfos;
	private final int offset_delta;
	
	public SameLocals1StackItemFrameExtend(short frameType, ClassReadCursor cursor) throws IOException {
		super(frameType);
		offset_delta = cursor.readUnsignedShort();
		verificationTypeInfos = new VerificationTypeInfo[1];
		verificationTypeInfos[0] = VerificationFactory.create(cursor.readUnsignedByte(),cursor);
	}
}
