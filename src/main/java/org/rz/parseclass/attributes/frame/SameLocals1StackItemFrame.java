package org.rz.parseclass.attributes.frame;

import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.attributes.typeinfo.VerificationFactory;
import org.rz.parseclass.attributes.typeinfo.VerificationTypeInfo;

import java.io.IOException;

public class SameLocals1StackItemFrame extends StackMapFrame {
	private final VerificationTypeInfo[] verificationTypeInfos;
	
	public SameLocals1StackItemFrame(short frameType, ClassReadCursor cursor) throws IOException {
		super(frameType);
		verificationTypeInfos = new VerificationTypeInfo[1];
		verificationTypeInfos[0] = VerificationFactory.create(cursor.readUnsignedByte(),cursor);
	}
}
