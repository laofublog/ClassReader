package org.rz.parseclass.attributes.frame;

import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.attributes.typeinfo.VerificationFactory;
import org.rz.parseclass.attributes.typeinfo.VerificationTypeInfo;

import java.io.IOException;

public class AppendFrame extends StackMapFrame {
	private final int offset_delta;
	private VerificationTypeInfo[] verificationTypeInfos;
	
	public AppendFrame(short frameType, ClassReadCursor cursor) throws IOException {
		super(frameType);
		offset_delta = cursor.readUnsignedShort();
		int verLength = frameType - 251;
		if (verLength > 0) {
			verificationTypeInfos = new VerificationTypeInfo[verLength];
			for (int i = 0; i < verLength; i++) {
				int tag=cursor.readUnsignedByte();
				verificationTypeInfos[i]= VerificationFactory.create(tag,cursor);
			}
		}
		
	}
}
