package org.rz.parseclass.attributes;

import org.rz.parseclass.ClassReadCursor;
import org.rz.parseclass.attributes.frame.FrameFactory;
import org.rz.parseclass.attributes.frame.StackMapFrame;

import java.io.IOException;

import static org.rz.parseclass.attributes.AttributeType.StackMapTable;

public class StackMapTable_attribute extends Attribute
{
	private int attribute_name_index;
	private int attribute_length;
	private int number_of_entries;
	private StackMapFrame[] stackMapFrames;
	public StackMapTable_attribute(ClassReadCursor cursor,int attrTag) throws IOException {
		super(StackMapTable, attrTag, cursor.readInt());
		number_of_entries = cursor.readUnsignedShort();
		//todo:此处还不知道到怎么转换
		//		String intNext = cusor.getHexNext(attribute_length-2);
		if (number_of_entries > 0) {
			stackMapFrames = new StackMapFrame[number_of_entries];
			for (int i = 0; i < number_of_entries; i++) {
				short frameType =(short) cursor.readUnsignedByte();
				StackMapFrame stack_map_frame = FrameFactory.create(frameType,cursor);
				stackMapFrames[i]=stack_map_frame;
			}
		}
	}
}


