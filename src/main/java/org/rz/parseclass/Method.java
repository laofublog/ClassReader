package org.rz.parseclass;

import org.rz.parseclass.attributes.Attribute;
import org.rz.parseclass.attributes.AttributeFactory;

import java.util.Set;
import java.util.stream.Collectors;

public class Method {
	private String access_flags;
	private int name_index;
	private int descriptor_index;
	private int attributes_count;
	private Attribute attributes[];
	
	
	
	public Method(ClassReadCursor cursor) throws Exception {
		Set<String> strSet = new AccessFlags(cursor).getMethodFlags();
		access_flags = strSet.stream().collect(Collectors.joining(","));
		name_index = cursor.readUnsignedShort();
		descriptor_index = cursor.readUnsignedShort();
		attributes_count = cursor.readUnsignedShort();
		if (attributes_count > 0) {
			attributes = new Attribute[attributes_count];
			for (int i = 0; i < attributes_count; i++) {
				attributes[i] = AttributeFactory.create(cursor);
			}
		}
	}
	
	public String getAccess_flags() {
		return access_flags;
	}
	
	public int getName_index() {
		return name_index;
	}
	
	public int getDescriptor_index() {
		return descriptor_index;
	}
	
	public int getAttributes_count() {
		return attributes_count;
	}
	
	public Attribute[] getAttributes() {
		return attributes;
	}
}
