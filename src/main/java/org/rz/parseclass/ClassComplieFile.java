package org.rz.parseclass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ClassComplieFile {
	private String filePath;
	private String classHexCnt;
	
	public ClassComplieFile(String path) {
		if (!path.contains(".class")) {
			path += ".class";
		}
		this.filePath = path;
		classHexCnt = readClassFileToStr();
	}
	
	public String getClassHexCnt() {
		return classHexCnt;
	}
	
	
	private String readClassFileToStr() {
		InputStream is = null;
		File file = new File(filePath);
		if (!file.exists()){
			System.out.println("文件不存在");
			return "";
		}
		try {
			is = new FileInputStream(file);
			int tmp = 0;
			StringBuilder sb = new StringBuilder("");
			while ((tmp = is.read()) != -1) {
				String strHex = Integer.toHexString(tmp & 0xFF);
				sb.append((strHex.length() == 1) ? "0" + strHex : strHex); // 每个字节由两个字符表示，位数不够，高位补0
			}
			return sb.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "";
	}
	
	
	private String readClass() {
		InputStream is = null;
		File file = new File(filePath);
		if (!file.exists()){
			System.out.println("文件不存在");
			return "";
		}
		try {
			is = new FileInputStream(file);
			int tmp = 0;
			StringBuilder sb = new StringBuilder("");
			while ((tmp = is.read()) != -1) {
				String strHex = Integer.toHexString(tmp & 0xFF);
				sb.append((strHex.length() == 1) ? "0" + strHex : strHex); // 每个字节由两个字符表示，位数不够，高位补0
			}
			return sb.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "";
	}
	
}
