package org.rz;

public class AppMain {
	
	private String userName;
	private int age;
	private short sex;
	private long userCode;
	private double income;
	private float ablity;
	private final static String userId="000000001";
	private final long indentityCardNo=34122202019029L;
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public short getSex() {
		return sex;
	}
	
	public void setSex(short sex) {
		this.sex = sex;
	}
	
	public long getUserCode() {
		return userCode;
	}
	
	public void setUserCode(long userCode) {
		this.userCode = userCode;
	}
	
	public double getIncome() {
		return income;
	}
	
	public float getAblity() {
		return ablity;
	}
	
	public void setAblity(float ablity) {
		this.ablity = ablity;
	}
	
	public static String getUserId() {
		return userId;
	}
	
	public long getIndentityCardNo() {
		return indentityCardNo;
	}
	
	
	public String getUserInfo() {
		return "AppMain{" + "userName='" + userName + '\'' + ", age=" + age + ", sex=" + sex + ", userCode=" + userCode + ", income=" + income + ", ablity=" + ablity + ", indentityCardNo=" + indentityCardNo + '}';
	}
	
	private static AppMain appmain;
	
	public static AppMain getInstance() {
		synchronized (AppMain.class) {
			if (appmain == null) {
				appmain = new AppMain();
			}
			return appmain;
		}
	}
	
	public synchronized boolean changeName(String name) {
		if (StringUtils.isEmpty(name)) {
			return false;
		}
		if (StringUtils.isEmpty(this.getUserName())) {
			this.setUserName(name);
		}
		return true;
	}
	
	private void testMain(){
		System.out.println("init ok");
	}
}
