package org.rz;

public class MyObject {
	public static int staticVar;
	public int instanceVar;
	
	public static void main(String[] args) {
		int x = 32768; // ldc MyObject myObj = new MyObject();
		MyObject myObject = new MyObject();
		MyObject.staticVar = x;
		myObject.instanceVar = x;
		x = myObject.instanceVar;
		Object obj = myObject;
		if (obj instanceof MyObject) {
			myObject = (MyObject) obj;
			System.out.println(myObject.instanceVar);
		}
	}
	
}

