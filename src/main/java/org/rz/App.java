package org.rz;

import com.sun.tools.classfile.ClassFile;
import com.sun.tools.classfile.ConstantPoolException;

import java.io.File;
import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        File file=new File("target/classes/org/rz/HelloWorld.class");
        try {
            ClassFile read = ClassFile.read(file);
            System.out.println(read.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ConstantPoolException e) {
            e.printStackTrace();
        }
    }
}
